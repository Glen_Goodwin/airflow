from airflow import DAG
import datetime as dt
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.operators.bash import BashOperator

dag = DAG(
    dag_id="carbon_commitment",
    schedule_interval="@monthly",
    start_date=dt.datetime(year=2021, month=5, day=11),
    catchup=False,
)

get_data = BashOperator(
    task_id="get_data",
    bash_command="curl 'https://filesamples.com/samples/document/csv/sample4.csv' > /tmp/test.csv",
    dag=dag,
)

does_exist = FileSensor(
    task_id="does_exist", poke_interval=30, filepath="/tmp/test.csv"
)
#does_exist2 = FileSensor(
#   task_id="does_exist2", poke_interval=30, filepath="/opt/airflow/data"
#)
get_data >> does_exist
