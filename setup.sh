#!/bin/sh

python3 -m venv venv
source venv/bin/activate

pip install wheel apache-airflow

export AIRFLOW__CORE__LOAD_EXAMPLES=False

airflow db init

airflow users create \
    --username admin \
    --role Admin \
    --email user@example.com \
    --firstname firstname \
    --lastname lastname \
    --password admin

airflow scheduler --daemon
airflow webserver --daemon

echo To login visit http://localhost:8080 and use login admin/admin
